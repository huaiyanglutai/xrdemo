<?php

class pwd extends \Xunruicms
{
	public function index()
	{


	    if (IS_POST) {

	        $data = $_POST['data'];
	        if (!$data['username']) {
	            $this->_json(0, '账号不能为空');
            } elseif (!$data['password']) {
	            $this->_json(0, '密码不能为空');
            }

            $db = [];
            $mysqli = function_exists('mysqli_init') ? mysqli_init() : 0;
            require ROOTPATH.'config/database.php';

            if (!$mysqli) {
                $this->_json(0, 'PHP环境必须启用Mysqli扩展');
            } elseif (!mysqli_real_connect($mysqli, $db['default']['hostname'], $db['default']['username'], $db['default']['password'])) {
                $this->_json(0, '['.mysqli_connect_errno().'] - 无法连接到数据库服务器（'.$db['default']['hostname'].'），
                请检查用户名（'.$db['default']['username'].'）和密码（'.$db['default']['password'].'）是否正确');
            } elseif (!mysqli_select_db($mysqli, $db['default']['database'])) {
                $this->_json(0, '指定的数据库（'.$db['default']['database'].'）不存在');
            }

            $rs = mysqli_query($mysqli, 'select * FROM `'.$db['default']['DBPrefix'].'member` where username="'.$data['username'].'"');
            $user = mysqli_fetch_array($rs,MYSQLI_NUM);
            if (!$user) {
                $this->_json(0, '账号不存在');
            }
            $salt = substr(md5(rand(0, 999)), 0, 10);
            $data['password'] = trim($data['password']);

            mysqli_query($mysqli, 'update `'.$db['default']['DBPrefix'].'member` set `password`="'.md5(md5($data['password']).$salt.md5($data['password'])).'",`salt`="'.$salt.'" where username="'.$data['username'].'"');


            $this->_json(1, '重置密码成功');
        }

        $form = '<input name="is_form" type="hidden" value="1">'.PHP_EOL;
        $form.= '<input name="is_tips" type="hidden" value="">'.PHP_EOL;

        $this->template->assign([
            'form' => $form,
            'class' => 'pwd',
            'meta_title' => '重置密码',
        ]);
        $this->template->display('pwd.html');exit;
	}
}
