<?php

class login extends Xunruicms
{
	public function index()
	{


	    if (IS_POST) {

            $pwd = require WEBPATH.'config/pwd.php';
            if (!$pwd) {
                $this->_json(0, '后台没有设置登录密码');
            }

            $post = $this->post('pwd');
            if ($post != $pwd) {
                $this->_json(0, '工具箱密码不正确（初始密码是admin，可以在CMS后台进行变更密码）');
            }

            $_SESSION["admin"] = 1;
            $this->_json(1, '登录成功');
        }

        $form = '<input name="is_form" type="hidden" value="1">'.PHP_EOL;
        $form.= '<input name="is_tips" type="hidden" value="">'.PHP_EOL;

        $this->template->assign([
            'form' => $form,
            'meta_title' => '登录工具箱',
        ]);
        $this->template->display('login.html');exit;
	}
}
