<?php


class home extends Xunruicms {

    /**
     * 首页
     */
    public function index() {

        $this->template->assign(array(
            'class' => 'home',
        ));
        $this->template->display('index.html');
    }

}
