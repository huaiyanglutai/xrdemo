<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 站点配置文件
 */

return array (
    1 => array (
        'SITE_NAME' => '我的网站',
        'SITE_DOMAIN' => 'xr.cc',
        'SITE_LOGO' => 'http://xr.cc/static/assets/logo-web.png',
        'SITE_MOBILE' => '',
        'SITE_MOBILE_DIR' => 'mobile',
        'SITE_AUTO' => '',
        'SITE_IS_MOBILE_HTML' => '',
        'SITE_MOBILE_NOT_PAD' => '',
        'SITE_CLOSE' => NULL,
        'SITE_THEME' => NULL,
        'SITE_TEMPLATE' => NULL,
        'SITE_REWRITE' => NULL,
        'SITE_SEOJOIN' => NULL,
        'SITE_LANGUAGE' => NULL,
        'SITE_TIMEZONE' => NULL,
        'SITE_TIME_FORMAT' => NULL,
        'SITE_INDEX_HTML' => '',
        'SITE_THUMB_WATERMARK' => 0,
    ),
);