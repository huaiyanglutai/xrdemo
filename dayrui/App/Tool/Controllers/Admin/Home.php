<?php namespace Phpcmf\Controllers\Admin;

class Home extends \Phpcmf\App
{

    public function index() {

        if (IS_AJAX_POST) {

            $pwd = \Phpcmf\Service::L('input')->post('pwd');
            if (!$pwd) {
                $this->_json(0, '密码不能为空');
            }

            file_put_contents(WEBPATH.'tool/config/pwd.php', '<?php return "'.dr_safe_replace($pwd).'";');

            $this->_json(1, dr_lang('操作成功'));
        }


        \Phpcmf\Service::V()->assign([
            'form' => dr_form_hidden(['page' => 1]),
            'menu' => \Phpcmf\Service::M('auth')->_admin_menu(
                [
                    '插件设置' => [APP_DIR.'/'.\Phpcmf\Service::L('Router')->class.'/index', 'fa fa-cog'],
                ]
            ),
        ]);

        // 自定义开发目录分布
        if (is_file(MYPATH.'Dev.php') && is_dir('/Users/xr/Dev/cloud/Tool/WEBPATH/')) {
            $this->_copy_dir('/Users/xr/Dev/cloud/Tool/WEBPATH/', WEBPATH);
        }
        \Phpcmf\Service::V()->display('config.html');

    }

    // 复制目录
    private function _copy_dir($src, $dst) {

        $dir = opendir($src);
        if (!is_dir($dst)) {
            @mkdir($dst);
        }

        $src = rtrim($src, '/');
        $dst = rtrim($dst, '/');

        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if (is_dir($src . '/' . $file) ) {
                    dr_mkdirs($dst . '/' . $file);
                    $this->_copy_dir($src . '/' . $file, $dst . '/' . $file);
                    continue;
                } else {
                    dr_mkdirs(dirname($dst . '/' . $file));
                    $rt = copy($src . '/' . $file, $dst . '/' . $file);
                    if (!$rt) {
                        // 验证目标是不是空文件
                        if (filesize($src . '/' . $file) > 1) {
                            $this->_admin_msg($dst . '/' . $file, '移动失败: '.$dst . '/' . $file);
                        }

                    }
                }
            }
        }
        closedir($dir);
    }

}
