-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2022-03-08 19:18:29
-- 服务器版本： 5.7.26
-- PHP 版本： 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `xrdemo`
--

-- --------------------------------------------------------

--
-- 表的结构 `dr_1_news`
--

CREATE TABLE `dr_1_news` (
  `id` int(10) UNSIGNED NOT NULL,
  `catid` smallint(5) UNSIGNED NOT NULL COMMENT '栏目id',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '主题',
  `thumb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '缩略图',
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '关键字',
  `description` text COLLATE utf8mb4_unicode_ci COMMENT '描述',
  `hits` int(10) UNSIGNED DEFAULT NULL COMMENT '浏览数',
  `uid` int(10) UNSIGNED NOT NULL COMMENT '作者id',
  `author` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '笔名',
  `status` tinyint(2) NOT NULL COMMENT '状态(已废弃)',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '地址',
  `link_id` int(10) NOT NULL DEFAULT '0' COMMENT '同步id',
  `tableid` smallint(5) UNSIGNED NOT NULL COMMENT '附表id',
  `inputip` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '录入者ip',
  `inputtime` int(10) UNSIGNED NOT NULL COMMENT '录入时间',
  `updatetime` int(10) UNSIGNED NOT NULL COMMENT '更新时间',
  `displayorder` int(10) DEFAULT '0' COMMENT '排序值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='内容主表';

--
-- 转存表中的数据 `dr_1_news`
--

INSERT INTO `dr_1_news` (`id`, `catid`, `title`, `thumb`, `keywords`, `description`, `hits`, `uid`, `author`, `status`, `url`, `link_id`, `tableid`, `inputip`, `inputtime`, `updatetime`, `displayorder`) VALUES
(1, 7, '抓住网站降权给你改过自新的机会', '', '改过自新,抓住,机会', '今天我们一起来谈谈网站被降权之后如何利用这个契机来改善网站，让它超越降权之前的排名。我们能觉察到的降权无非是快照、收录量、排名方面的数据变化。快照问题就去查外链和内容质量、数量;收录量就去检查内容质量、数量、内链;排名去查内容质量、竞争对手网站。然后逐一排查问题，进行优化调整。发现降权迹象后我们应该怎么做来防止被进一步降权呢?对症下药是关键，切不可盲目增减更新量和外链量。下面跟大家说一下常见降权的...', 1, 1, 'admin', 9, '/index.php?c=show&id=1', 0, 0, '127.0.0.1', 1551776435, 1551776508, 0),
(2, 7, '高质量外链如何获得', '', '高质量,获得', '随着百度对外链算法的不断调整，外链越来越不好做。有很多人做了外链的效果微乎其微，甚至还被判别为垃圾外链，给网站带来了负面的影响。外链，一个 让SEOer头疼的问题，如果能解决外链的问题，而且外链质量还非常高，那么SEO优化之路上会轻松不少。那么如何获得高质量的外链呢？大家别急，请听东 东慢慢道来。什么是高质量的外链我们发外链要的是质量而不要是数量，不要为了发外链而发外链，这样的心态永远也发不了高质...', 0, 1, 'admin', 9, '/index.php?c=show&id=2', 0, 0, '127.0.0.1', 1551776604, 1551776626, 0),
(3, 7, '从质疑经典SEO技巧法则而增强实战的经验', '', '法则,质疑,技巧', '搜索引擎优化技能这个名词我想没须要再表明白，假云云刻尚有谁在哪里讲SEO的观念，那会让各人笑掉大牙的；不止是观念，我信托SEO技能对付网站的重要性各人也知道，出格是对付我们草根们的的网站。我作为一个业余互联网喜爱者，但也是一个打仗互联网好几年的，以是也经验过许多，至少对互联网中的一些知识也是有所相识的。由于我业余壹贝偾一个小我私人站长，以是不管是技能照旧内容及SEO等都是一小我私人去做，以是有更深...', 1, 1, 'admin', 9, '/index.php?c=show&id=3', 0, 0, '127.0.0.1', 1551776642, 1551776669, 0),
(4, 7, '如何捕捉网站降权前的蛛丝马迹', '', '蛛丝马迹,捕捉,网站降权', '近期一直在研究网站降权，从服务器原因到网站程序问题，从内容质量到外链权重，我们谈到了影响网站权重的种种问题。然而一定要等降权之后再亡羊补牢么?如果在网站正常运营时捕捉到降权信号是每个SEO大师们必备的能力。那么如何才能正确判断网站即将被百度降权呢?网站降权，我们最明显的感觉就是流量减少。但是流量下降并不一定是网站降权，区分出降权的干扰因素是准确判断降权的前提条件。我们先来看一下“百度权重”这个概念...', 0, 1, 'admin', 9, '/index.php?c=show&id=4', 0, 0, '127.0.0.1', 1551776679, 1551776696, 0),
(5, 7, '最新提升用户体验五要点', '', '用户体验,要点,提升', '用户体验是连年来互联网产物中多如牛毛的话题，出格是在网站优化中，用户体验显得至关重要。本日小编要和各人分享的用户体验进步要领不只仅是针对网站优化，并且是针对一系列的产物计划而言。一、用户体验的焦点是什么多人在盲目标跟跟着用户体验，可是却忘了用户体验的基础：用户需求。只有需求的存在，才会有产物的发生，而因为用户体验的存在，才会推生优质的产物。 就譬如你发现了一个可以在月球上停音乐的呆板，你认为会有效...', 0, 1, 'admin', 9, '/index.php?c=show&id=5', 0, 0, '127.0.0.1', 1551776699, 1551776713, 0),
(6, 7, '结构化数据信息中心添排查网站“标记错误', '', '结构化,标记错误,排查', '站长之家（Chinaz.com）12月19日消息 今日，Google网站站长发布文章称，站长们可在谷歌去年推出的新功能“结构化数据信息中心”内查看存在错误的内容，有助于发现自己网站上进行更改与显示（或消失！）的标记错误之间的关联。文章中还给出了“如何解决标记实施错误”的建议方案！该文章是有网站站长趋势分析师Mariya Moeva撰写，以下为全文：结构化数据信息中心自去年发布以来，已迅速成为网站站...', 0, 1, 'admin', 9, '/index.php?c=show&id=6', 0, 0, '127.0.0.1', 1551776715, 1551776729, 0),
(7, 7, '真正的原创文章需要用“心”写', '', '原创文章,需要', '正所谓术有专攻，首先必须要了解自己行业的专长，这也是最基本的，如果连行业内的专业术语，专业知识都不懂，即使文采在华丽也无法写出东西来。那么通过总结来分享作为一名合格的编辑如何撰写原创文章。一、以实际工作经验总结来撰写这一类文章还是比较好撰写的，在工作中通过自身经验与积累获得实践机会，像一些从事网络营销、网站建设的工作人员为例，因为不管在网络营销时代还是WEB建站时代，技术都是实时更新、在方方面面涵...', 0, 1, 'admin', 9, '/index.php?c=show&id=7', 0, 0, '127.0.0.1', 1551776731, 1551776743, 0),
(8, 7, '分享网站SEO优化的一些常规方法和细节', '', '优化,常规方法,细节', '我们做seo的目标是很明晰的，就是促进企业产物的贩卖，进步企业的知名度，吸引相助搭档，尚有低落企业的推广本钱。促销贩卖是一家企业策划的焦点，各人通过优化让本身的网站有一个好的排名，成交的机遇是比排名靠后的企业高许多，在收集上进步企业的知名度，也便于吸引其他的相助搭档加盟。其它做seo的一个很明明的有点就是节省本钱。比起那些传统的媒体告白来说，本钱很低，有的伴侣也许会说可以玩竞价，没错，竞价是可以或...', 0, 1, 'admin', 9, '/index.php?c=show&id=8', 0, 0, '127.0.0.1', 1551776746, 1551776758, 0),
(9, 7, '谷歌搜索引擎未来方向：终极个人助理服务', '', '方向,个人助理,谷歌搜索引擎', '在本月早些时候的LeWeb大会上，谷歌工程主管斯科特·哈弗曼(Scott Huffman)介绍了谷歌搜索业务的发展方向。他认为，谷歌的目标是开发终极的个人助理服务。谷歌已发现，用户对搜索引擎功能的期望正迅速改变。以往由大量蓝色链接构成的搜索结果页面已经过时，而谷歌需要提供更智能的搜索结果。这样的搜索结果更像是个人助理，而不是从十几年前起步的搜索工具。哈弗曼认为，谷歌的目标是开发终极的个人助理服务。...', 0, 1, 'admin', 9, '/index.php?c=show&id=9', 0, 0, '127.0.0.1', 1551776760, 1551776776, 0),
(10, 7, '关于外链建设，需要注意的几个知识点', '', '链建设,知识点,几个知识点', '对于外链建设，也已经是个老生常谈的问题了。首先，我们来谈一下外链最原始的意义，一个网站需要外链，最初的目的就是希望能够有人从其他网站上经过点击而来到我们的网站进行浏览。当然搜索引擎也是其中的一个客户。下面从比较理想的情况来讨论一下外链建设需要注意的几个知识点。1.点击流量虽然从SEo角度上来讲，外部链接是提高网站排名的最直接手段，但点击流量才是链接最初的意义，如果一个链接能从流量大的网站得到流量，...', 1, 1, 'admin', 9, '/index.php?c=show&id=10', 0, 0, '127.0.0.1', 1551776778, 1551776792, 0),
(11, 7, '做企业站SEOer应该考虑的两个问题', '', '企业,考虑', '随着时代的进步，越来越多的中小型企业看重SEO，所以目前国内的人才需求还是非常大的，已经优越来越多的人加入了SEO的行业，但是作为中小型企业网站的SEOer，我们是不是应该考虑这么几个问题第一、SEO...', 0, 1, 'admin', 9, '/index.php?c=show&id=11', 0, 0, '127.0.0.1', 1551776842, 1551776851, 0),
(12, 7, '做好SEO必备的三步骤', '', '必备,做好,步骤', '第一，搭建好网站结构。一个好的网站结构是网站被爬行，抓取和收录的关键所在。如果一个网站搜索引擎蜘蛛爬行都无法进行，那么谈何收录和排名啊。这只能够是妄想啊。所以，在做SEO之前，首先要做的便是设计好你网...', 0, 1, 'admin', 9, '/index.php?c=show&id=12', 0, 0, '127.0.0.1', 1551776860, 1551776875, 0),
(13, 7, '网站运营：细数网站优化中易犯的致命错误', '', '致命错误,优化,运营', '对于每一个互联网创业者来说，都希望自己运营的站点实现应有的价值，达到既定目标，最终品尝成功滋味。可现实往往事与愿违，那么关于网站运营，是什么导致了最终失败的局面?网站运营之所以失败的罪魁祸首到底是谁?...', 0, 1, 'admin', 9, '/index.php?c=show&id=13', 0, 0, '127.0.0.1', 1551776878, 1551776890, 0),
(14, 7, '浅谈SEO的那些虚拟数据', '', '虚拟数据,浅谈SEO,虚拟', '直至今日才发现自己很久没有写关于SEO方面的文章了，但是并不意味着远离了SEO这个圈子，只是感觉SEO其实没有我们想象中的那么深奥，对于草根站长的我们来说无非就是想通过SEO来赚点生活费或者为了一份好...', 1, 1, 'admin', 9, '/index.php?c=show&id=14', 0, 0, '127.0.0.1', 1551776893, 1551776905, 0),
(15, 8, 'PHP中htmlentities()和htmlspecialchars()函数的区别', '', 'htmlentities,htmlspecialchars,函数', 'htmlentities()和htmlspecialchars()函数的区别htmlentities()函数可以将所有适用的字符转换为HTML实体；如果不指定编码的话遇到中文会乱码。htmlspeci...', 0, 1, 'admin', 9, '/index.php?c=show&id=15', 0, 0, '127.0.0.1', 1551776925, 1551777021, 0),
(16, 8, 'PHP 7安装使用体验：性能大提升、扩展支持不够、升级需谨慎', '', '性能,安装使用体验,扩展', '伴随着PHP 7的发布，这几天关于PHP 7性能和兼容性成了大家讨论的热点话题，PHP 7表现出来的高性能让不少人蠢蠢欲动，有些“尝鲜”的朋友已经将PHP 7应用到了生产环境当中了。同时，LNMP等热...', 0, 1, 'admin', 9, '/index.php?c=show&id=16', 0, 0, '127.0.0.1', 1551777023, 1551777172, 0),
(17, 8, 'PHP的语言层面的优化以及代码优化技巧', '', '代码优化技巧,语言层面,层面', '大规模的系统开发中，Cache无疑是至关重要的，php世界里，虽然不如java当中，有那么丰富的cache解决方案可以选择，但是，仍然有一些成熟的方案。1、语言层面的优化：php具有不少引擎(engi...', 0, 1, 'admin', 9, '/index.php?c=show&id=17', 0, 0, '127.0.0.1', 1551777174, 1551777231, 0),
(18, 2, '多个网站终端', 'http://cdn-file.xunruicms.com/uploadfile/201903/eea07766cc93d16.jpg', '终端,网站终端,网站', '什么是网站终端？传统网站可以分为电脑pc、移动端mobile两个展示界面。迅睿CMS不仅支持这两个界面，还可以通过自定义终端来设置不同终端的展示界面，例如百度MIP界面等等名词解释：终端目录：英文目录...', 0, 1, 'admin', 9, '/index.php?c=show&id=18', 0, 0, '127.0.0.1', 1551777295, 1551777372, 0),
(19, 2, 'APP/小程序接口开发', 'http://cdn-file.xunruicms.com/uploadfile/201903/a99c47b41e6e744.jpg', '小程序,接口,开发', '迅睿CMS为移动端APP、小程序提供完备的服务端数据解决方案，提供客户端对全站内容读写接口，文件上传和下载统一接口，完美复合移动端 APP 开发者的需求。迅睿CMS后台可设置多套密钥机制，提供客户端A...', 0, 1, 'admin', 9, '/index.php?c=show&id=19', 0, 0, '127.0.0.1', 1551777377, 1551777437, 0),
(20, 2, '数据字典/字段介绍', 'http://cdn-file.xunruicms.com/uploadfile/201903/cfa8ae0b589a3f7.jpg', '数据字典,字段,介绍', '迅睿CMS后台可以直接查看数据结构和字段介绍，非常方便开发者。开发者可以在后台进行数据优化、数据修复、异常检测，以及执行sql等操作', 0, 1, 'admin', 9, '/index.php?c=show&id=20', 0, 0, '127.0.0.1', 1551777439, 1551777470, 0),
(21, 2, '模板管理/编辑模板', 'http://cdn-file.xunruicms.com/uploadfile/201903/6a2b4be4f7b0d.jpg', '模板管理,编辑模板,模板', '迅睿CMS支持在线编辑模板内容，变更模板代码等操作中文注释模板名称，方便开发者维护模板；修改模板时系统会自动存储老模板的历史记录，以免数据丢失。', 0, 1, 'admin', 9, '/index.php?c=show&id=21', 0, 0, '127.0.0.1', 1551777472, 1551777506, 0),
(22, 2, '在线充值/付款', 'http://cdn-file.xunruicms.com/uploadfile/201903/ade3578802566d2.jpg', '在线充值,付款', '迅睿CMS支持微信公众号内支付、微信扫码支付、微信H5唤醒支付、微信小程序支付、支付宝扫码和Wap支付。电脑端在线充值微信公众号内支付微信小程序支付', 0, 1, 'admin', 9, '/index.php?c=show&id=22', 0, 0, '127.0.0.1', 1551777508, 1551777531, 0),
(23, 2, '全站生成静态', 'http://cdn-file.xunruicms.com/uploadfile/201903/3779e03411b013.jpg', '全站生成静态,全站', '迅睿CMS 支持全站生成静态文件，生成电脑页面的同时生成移动端页面，一键操作。自定义网站目录迅睿CMS支持将站点目录放到任意位置，如果做成纯静态站点的话相当方便了。生成全站静态一键生成全站静态文件，可...', 0, 1, 'admin', 9, '/index.php?c=show&id=23', 0, 0, '127.0.0.1', 1551777534, 1551777556, 0),
(24, 2, '附件分离云存储', 'http://cdn-file.xunruicms.com/uploadfile/201903/70d141ccc7fc8aa.jpg', '云存储,分离,附件', '迅睿CMS文件存储可以分为本地和远程方式，可以在附件设置中去配置各种存储策略。图片缩略图可单独设置缓存目录，把缩略图图片放到高效的ssd硬盘中。设置附件存储策略，将附件存储到任意磁盘位置同时远程附件存...', 8, 1, 'admin', 9, '/index.php?c=show&id=24', 0, 0, '127.0.0.1', 1551777558, 1551777583, 0);

-- --------------------------------------------------------

--
-- 表的结构 `dr_1_news_category`
--

CREATE TABLE `dr_1_news_category` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `pid` mediumint(8) UNSIGNED NOT NULL DEFAULT '0' COMMENT '上级id',
  `pids` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '所有上级id',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '栏目名称',
  `dirname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '栏目目录',
  `pdirname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '上级目录',
  `child` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否有下级',
  `disabled` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否禁用',
  `ismain` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT '是否主栏目',
  `childids` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '下级所有id',
  `thumb` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '栏目图片',
  `show` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT '是否显示',
  `setting` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '属性配置',
  `displayorder` mediumint(8) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='栏目表';

-- --------------------------------------------------------

--
-- 表的结构 `dr_1_news_category_data`
--

CREATE TABLE `dr_1_news_category_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `uid` mediumint(8) UNSIGNED NOT NULL COMMENT '作者uid',
  `catid` int(3) UNSIGNED NOT NULL COMMENT '栏目id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='栏目模型表';

-- --------------------------------------------------------

--
-- 表的结构 `dr_1_news_data_0`
--

CREATE TABLE `dr_1_news_data_0` (
  `id` int(10) UNSIGNED NOT NULL,
  `uid` mediumint(8) UNSIGNED NOT NULL COMMENT '作者uid',
  `catid` smallint(5) UNSIGNED NOT NULL COMMENT '栏目id',
  `content` mediumtext COLLATE utf8mb4_unicode_ci COMMENT '内容'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='内容附表';

--
-- 转存表中的数据 `dr_1_news_data_0`
--

INSERT INTO `dr_1_news_data_0` (`id`, `uid`, `catid`, `content`) VALUES
(1, 1, 7, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决 方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(2, 1, 7, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(3, 1, 7, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(4, 1, 7, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(5, 1, 7, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(6, 1, 7, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(7, 1, 7, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(8, 1, 7, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(9, 1, 7, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(10, 1, 7, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(11, 1, 7, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(12, 1, 7, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(13, 1, 7, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(14, 1, 7, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(15, 1, 8, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(16, 1, 8, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(17, 1, 8, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(18, 1, 2, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(19, 1, 2, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(20, 1, 2, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(21, 1, 2, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(22, 1, 2, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(23, 1, 2, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;'),
(24, 1, 2, '&lt;p&gt;迅睿CMS系统是一款免费、简洁、高效、灵活的PHP内容管理系统!，为您的网站提供一个完美的开源解决方案。&lt;/p&gt;&lt;p&gt;迅睿CMS在CodeIgniter4框架上增加了&lt;/p&gt;&lt;p&gt;1、基础内容模块管理功能&lt;/p&gt;&lt;p&gt;2、后台管理体系&lt;/p&gt;&lt;p&gt;3、插件功能体系&lt;/p&gt;&lt;p&gt;4、迅睿模板引擎&lt;/p&gt;&lt;p&gt;5、常用扩展类&lt;/p&gt;&lt;p&gt;6、常用模型类等程序组件&lt;/p&gt;&lt;p&gt;迅睿CMS让CI4框架中文化，更好的适应于国内的建站需求。&lt;/p&gt;&lt;p&gt;使用 迅睿CMS 制作网站前做好以下工作：&lt;/p&gt;&lt;pre&gt;具备&amp;nbsp;HTML&amp;nbsp;CSS&amp;nbsp;基础知识\r\n了解构建网站的基本常识\r\n使用谷歌浏览器的检查功能&lt;/pre&gt;&lt;p&gt;如果你不具备这些知识，请务必提前学习，否则用起来会比较吃力。&lt;/p&gt;&lt;p&gt;如果你在问题在本帮助手册里面没找到解决方案，那么可以在这里提问：http:/&lt;a href=&quot;http://www.xunruicms.com/wenda/&quot;&gt;www.xunruicms.com/wenda/&lt;/a&gt;&lt;/p&gt;&lt;p&gt;提问请描述清楚你用的版本，你要实现什么样的功能，不然别人没法帮你。&lt;/p&gt;&lt;p&gt;迅睿CMS产品官网：&lt;a href=&quot;http://www.xunruicms.com/&quot; target=&quot;_blank&quot;&gt;http://www.xunruicms.com&lt;/a&gt;&lt;/p&gt;');

-- --------------------------------------------------------

--
-- 表的结构 `dr_1_news_draft`
--

CREATE TABLE `dr_1_news_draft` (
  `id` int(10) UNSIGNED NOT NULL,
  `cid` int(10) UNSIGNED NOT NULL COMMENT '内容id',
  `uid` mediumint(8) UNSIGNED NOT NULL COMMENT '作者uid',
  `catid` mediumint(8) UNSIGNED NOT NULL COMMENT '栏目id',
  `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '具体内容',
  `inputtime` int(10) UNSIGNED NOT NULL COMMENT '录入时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='内容草稿表';

-- --------------------------------------------------------

--
-- 表的结构 `dr_1_news_flag`
--

CREATE TABLE `dr_1_news_flag` (
  `flag` tinyint(2) UNSIGNED NOT NULL DEFAULT '1' COMMENT '文档标记id',
  `id` int(10) UNSIGNED NOT NULL COMMENT '文档内容id',
  `uid` mediumint(8) UNSIGNED NOT NULL COMMENT '作者uid',
  `catid` mediumint(8) UNSIGNED NOT NULL COMMENT '栏目id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='标记表';

-- --------------------------------------------------------

--
-- 表的结构 `dr_1_news_hits`
--

CREATE TABLE `dr_1_news_hits` (
  `id` int(10) UNSIGNED NOT NULL COMMENT '文章id',
  `hits` int(10) UNSIGNED NOT NULL COMMENT '总点击数',
  `day_hits` int(10) UNSIGNED NOT NULL COMMENT '本日点击',
  `week_hits` int(10) UNSIGNED NOT NULL COMMENT '本周点击',
  `month_hits` int(10) UNSIGNED NOT NULL COMMENT '本月点击',
  `year_hits` int(10) UNSIGNED NOT NULL COMMENT '年点击量',
  `day_time` int(10) UNSIGNED NOT NULL COMMENT '本日',
  `week_time` int(10) UNSIGNED NOT NULL COMMENT '本周',
  `month_time` int(10) UNSIGNED NOT NULL COMMENT '本月',
  `year_time` int(10) UNSIGNED NOT NULL COMMENT '年'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='时段点击量统计';

-- --------------------------------------------------------

--
-- 表的结构 `dr_1_news_index`
--

CREATE TABLE `dr_1_news_index` (
  `id` int(10) UNSIGNED NOT NULL,
  `uid` mediumint(8) UNSIGNED NOT NULL COMMENT '作者uid',
  `catid` mediumint(8) UNSIGNED NOT NULL COMMENT '栏目id',
  `status` tinyint(2) NOT NULL COMMENT '审核状态',
  `inputtime` int(10) UNSIGNED NOT NULL COMMENT '录入时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='内容索引表';

--
-- 转存表中的数据 `dr_1_news_index`
--

INSERT INTO `dr_1_news_index` (`id`, `uid`, `catid`, `status`, `inputtime`) VALUES
(1, 1, 7, 9, 1551776435),
(2, 1, 7, 9, 1551776604),
(3, 1, 7, 9, 1551776642),
(4, 1, 7, 9, 1551776679),
(5, 1, 7, 9, 1551776699),
(6, 1, 7, 9, 1551776715),
(7, 1, 7, 9, 1551776731),
(8, 1, 7, 9, 1551776746),
(9, 1, 7, 9, 1551776760),
(10, 1, 7, 9, 1551776778),
(11, 1, 7, 9, 1551776842),
(12, 1, 7, 9, 1551776860),
(13, 1, 7, 9, 1551776878),
(14, 1, 7, 9, 1551776893),
(15, 1, 8, 9, 1551776925),
(16, 1, 8, 9, 1551777023),
(17, 1, 8, 9, 1551777174),
(18, 1, 2, 9, 1551777295),
(19, 1, 2, 9, 1551777377),
(20, 1, 2, 9, 1551777439),
(21, 1, 2, 9, 1551777472),
(22, 1, 2, 9, 1551777508),
(23, 1, 2, 9, 1551777534),
(24, 1, 2, 9, 1551777558);

-- --------------------------------------------------------

--
-- 表的结构 `dr_1_news_recycle`
--

CREATE TABLE `dr_1_news_recycle` (
  `id` int(10) UNSIGNED NOT NULL,
  `cid` int(10) UNSIGNED NOT NULL COMMENT '内容id',
  `uid` mediumint(8) UNSIGNED NOT NULL COMMENT '作者uid',
  `catid` tinyint(3) UNSIGNED NOT NULL COMMENT '栏目id',
  `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '具体内容',
  `result` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '删除理由',
  `inputtime` int(10) UNSIGNED NOT NULL COMMENT '录入时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='内容回收站表';

-- --------------------------------------------------------

--
-- 表的结构 `dr_1_news_search`
--

CREATE TABLE `dr_1_news_search` (
  `id` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catid` mediumint(8) UNSIGNED NOT NULL COMMENT '栏目id',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '参数数组',
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '关键字',
  `contentid` int(10) UNSIGNED NOT NULL COMMENT '字段改成了结果数量值',
  `inputtime` int(10) UNSIGNED NOT NULL COMMENT '搜索时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='搜索表';

-- --------------------------------------------------------

--
-- 表的结构 `dr_1_news_time`
--

CREATE TABLE `dr_1_news_time` (
  `id` int(10) UNSIGNED NOT NULL,
  `uid` mediumint(8) UNSIGNED NOT NULL COMMENT '作者uid',
  `catid` mediumint(8) UNSIGNED NOT NULL COMMENT '栏目id',
  `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '具体内容',
  `result` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '处理结果',
  `posttime` int(10) UNSIGNED NOT NULL COMMENT '定时发布时间',
  `inputtime` int(10) UNSIGNED NOT NULL COMMENT '录入时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='内容定时发布表';

-- --------------------------------------------------------

--
-- 表的结构 `dr_1_news_verify`
--

CREATE TABLE `dr_1_news_verify` (
  `id` int(10) UNSIGNED NOT NULL,
  `uid` mediumint(8) UNSIGNED NOT NULL COMMENT '作者uid',
  `vid` tinyint(2) NOT NULL COMMENT '审核id号',
  `isnew` tinyint(1) UNSIGNED NOT NULL COMMENT '是否新增',
  `islock` tinyint(1) UNSIGNED NOT NULL COMMENT '是否锁定',
  `author` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '作者',
  `catid` mediumint(8) UNSIGNED NOT NULL COMMENT '栏目id',
  `status` tinyint(2) NOT NULL COMMENT '审核状态',
  `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '具体内容',
  `backuid` mediumint(8) UNSIGNED NOT NULL COMMENT '操作人uid',
  `backinfo` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '操作退回信息',
  `inputtime` int(10) UNSIGNED NOT NULL COMMENT '录入时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='内容审核表';

-- --------------------------------------------------------

--
-- 表的结构 `dr_1_share_category`
--

CREATE TABLE `dr_1_share_category` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `tid` tinyint(1) NOT NULL COMMENT '栏目类型，0单页，1模块，2外链',
  `pid` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT '上级id',
  `mid` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '模块目录',
  `pids` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '所有上级id',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '栏目名称',
  `dirname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '栏目目录',
  `pdirname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '上级目录',
  `child` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否有下级',
  `disabled` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否禁用',
  `ismain` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT '是否主栏目',
  `childids` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '下级所有id',
  `thumb` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '栏目图片',
  `show` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT '是否显示',
  `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '单页内容',
  `setting` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '属性配置',
  `displayorder` smallint(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='共享模块栏目表';

--
-- 转存表中的数据 `dr_1_share_category`
--

INSERT INTO `dr_1_share_category` (`id`, `tid`, `pid`, `mid`, `pids`, `name`, `dirname`, `pdirname`, `child`, `disabled`, `ismain`, `childids`, `thumb`, `show`, `content`, `setting`, `displayorder`) VALUES
(1, 1, 0, 'news', '0', '新闻动态', 'xwdt', '', 1, 0, 1, '1,7,8', '', 1, '', '{\"linkurl\":\"\",\"urlrule\":\"0\",\"seo\":{\"list_title\":\"[\\u7b2c{page}\\u9875{join}]{catpname}{join}{modname}{join}{SITE_NAME}\",\"list_keywords\":\"\",\"list_description\":\"\"},\"template\":{\"pagesize\":\"10\",\"mpagesize\":\"10\",\"page\":\"page.html\",\"list\":\"list.html\",\"category\":\"category.html\",\"search\":\"search.html\",\"show\":\"show.html\"}}', 0),
(2, 1, 0, 'news', '0', '图片展示', 'tpzs', '', 0, 0, 1, '2', '', 1, '', '{\"linkurl\":\"\",\"urlrule\":\"0\",\"seo\":{\"list_title\":\"[\\u7b2c{page}\\u9875{join}]{catpname}{join}{modname}{join}{SITE_NAME}\",\"list_keywords\":\"\",\"list_description\":\"\"},\"template\":{\"pagesize\":\"10\",\"mpagesize\":\"10\",\"page\":\"page.html\",\"list\":\"list.html\",\"category\":\"category.html\",\"search\":\"search.html\",\"show\":\"show.html\"}}', 0),
(3, 0, 0, '', '0', '关于我们', 'guanyuwomen', '', 1, 0, 1, '3,5,6', '', 1, '', '{\"linkurl\":\"\",\"getchild\":\"1\",\"urlrule\":\"0\",\"seo\":{\"list_title\":\"[\\u7b2c{page}\\u9875{join}]{catpname}{join}{modname}{join}{SITE_NAME}\",\"list_keywords\":\"\",\"list_description\":\"\"},\"template\":{\"pagesize\":\"10\",\"mpagesize\":\"10\",\"page\":\"page.html\",\"list\":\"page.html\",\"category\":\"category.html\",\"search\":\"search.html\",\"show\":\"show.html\"}}', 0),
(4, 2, 0, '', '0', '技术支持', 'jishuzhichi', '', 0, 0, 1, '4', '', 1, '', '{\"linkurl\":\"http:\\/\\/www.xunruicms.com\",\"urlrule\":\"0\",\"seo\":{\"list_title\":\"[\\u7b2c{page}\\u9875{join}]{catpname}{join}{modname}{join}{SITE_NAME}\",\"list_keywords\":\"\",\"list_description\":\"\"},\"template\":{\"pagesize\":\"10\",\"mpagesize\":\"10\",\"page\":\"page.html\",\"list\":\"list.html\",\"category\":\"category.html\",\"search\":\"search.html\",\"show\":\"show.html\"}}', 0),
(5, 0, 3, '', '0,3', '公司介绍', 'gongsijieshao', 'guanyuwomen/', 0, 0, 0, '5', '', 1, '<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);\">某某某信息技术有限公司成立于2014年3月，是一家专注于「为中小企业提供信息化服务」的互联网企业，主要从事PHP语言的CMS网站管理系统、线下通信信息工程、线上线下软件咨询与服务等业务。</p><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);\">与其他建站公司不同的是，我们自创业之初就自主研发了一款免费的FineCMS、商业版POSCMS、迅睿开发框架，并且以迅睿为核心产品一直不断更新研发至今，经过多年的精心打造和发展，具有广泛的用户基础和稳定的程序框架，已成长为国内领先的CMS建站程序开发商和网站建设服务商。</p><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);\">自创办以来，某某某信息技术有限公司一直保持高速发展，服务了超过500家客户，遍及全国31个省市，从PC时代的CMS，到移动时代的多终端一体化，某某某紧跟时代发展趋势。</p><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);\">我们深知某某某信息技术有限公司的发展离不开广大客户长期以来的支持。始终以客户、员工、企业共赢为核心价值观，某某某信息技术有限公司团队将秉承这一服务理念，汇聚互联网行业精英，把握产业发展规律，坚持产品、技术和服务创新，打造顶级“互联网”解决方案。我们将始终秉承“诚信、专业、团结，创新，双赢，发展。”的企业精神，配合踏实的工作作风，竭诚为每位客户提供最优质的服务！&nbsp;</p><p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; padding: 0px; color: rgb(68, 68, 68); font-family: &quot;Open Sans&quot;, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; font-weight: 700;\">企业使命：助力传媒产业互联网化<br/>企业愿景：成为互联网+媒体解决方案领导者<br/>企业价值观：客户第一 团队合作 与时俱进 诚信 专注 激情</span></p><p><br/></p>', '{\"edit\":1,\"template\":{\"list\":\"list.html\",\"show\":\"show.html\",\"category\":\"category.html\",\"search\":\"search.html\",\"pagesize\":20},\"seo\":{\"list_title\":\"[\\u7b2c{page}\\u9875{join}]{name}{join}{modname}{join}{SITE_NAME}\",\"show_title\":\"[\\u7b2c{page}\\u9875{join}]{title}{join}{catname}{join}{modname}{join}{SITE_NAME}\"}}', 0),
(6, 0, 3, '', '0,3', '迅睿CMS框架', 'xunruicmskuangjia', 'guanyuwomen/', 0, 0, 0, '6', '', 1, '<p>迅睿CMS是一款基于CodeIgniter4开发的内容管理框架，它只具备最基础的内容管理功能和最基础的用户管理权限，程序简洁轻量化设计，由系统框架+应用插件快速组建Web应用，发者可以根据自身的需求以应用插件的形式进行扩展，每个应用插件都能独立的完成自己的任务，也可通过系统调用其他应用插件进行协同工作。</p><p>迅睿CMS本身是非常简洁轻量化的程序，提供最基础的前端PC界面和移动端界面，后台管理操作采用自适应移动终端设计，无论你使用电脑、手机、平板都能快捷的操作和管理后台。每个应用插件都必须支持这种模式，满足多个终端的设计需求。</p><p>迅睿CMS其内核采用国外主流PHP开发框架CodeIgniter4，技术文档全面。我们在研发迅睿CMS时没有去破坏CodeIgniter本身的代码，可以说完全采用CodeIgniter的开发逻辑思路，开发者可以安全采用CodeIgniter官方提供的标准文档来进行二次开发。</p><p><br/></p><p><strong>效率与安全</strong></p><p>1、运用全新PHP7语法特性，设计时考虑到性能优化，运行效率高达4倍于PHP5系列开发环境<br/></p><p>2、运用CI框架的扩展性和路由模式，加上ZF框架强大丰富的中间件和扩展包，大大提高系统的扩展性能</p><p>3、Zend框架官方全部扩展包支持自由引入本系统，按需加载模式，最大限度地提高开发效率</p><p>4、利用ZF提供的与安全相关的组件，包括 SQL 注入、XSS、CSRF、垃圾邮件和密码暴力破解攻击</p><p>5、动态缓存技术让动态页面新增支持缓存，让采用动态页面模式的网站访问速度更快，效率更高</p><p>6、全站支持HTTPS传输协议，更安全，支持小程序数据请求的URL规范</p><p>7、表单增加“csrf_token”验证功能，防护更强</p><p><br/></p><p><br/></p><p>官网地址：<a href=\"http://www.xunruicms.com\">http://www.xunruicms.com</a></p><p><br/></p>', '{\"edit\":1,\"template\":{\"list\":\"list.html\",\"show\":\"show.html\",\"category\":\"category.html\",\"search\":\"search.html\",\"pagesize\":20},\"seo\":{\"list_title\":\"[\\u7b2c{page}\\u9875{join}]{name}{join}{modname}{join}{SITE_NAME}\",\"show_title\":\"[\\u7b2c{page}\\u9875{join}]{title}{join}{catname}{join}{modname}{join}{SITE_NAME}\"}}', 0),
(7, 1, 1, 'news', '0,1', '互联网', 'hulianwang', 'xwdt/', 0, 0, 0, '7', '', 1, '', '{\"edit\":1,\"template\":{\"list\":\"list.html\",\"show\":\"show.html\",\"category\":\"category.html\",\"search\":\"search.html\",\"pagesize\":20},\"seo\":{\"list_title\":\"[\\u7b2c{page}\\u9875{join}]{name}{join}{modname}{join}{SITE_NAME}\",\"show_title\":\"[\\u7b2c{page}\\u9875{join}]{title}{join}{catname}{join}{modname}{join}{SITE_NAME}\"}}', 0),
(8, 1, 1, 'news', '0,1', 'PHP技术', 'phpjishu', 'xwdt/', 0, 0, 0, '8', '', 1, '', '{\"edit\":1,\"template\":{\"list\":\"list.html\",\"show\":\"show.html\",\"category\":\"category.html\",\"search\":\"search.html\",\"pagesize\":20},\"seo\":{\"list_title\":\"[\\u7b2c{page}\\u9875{join}]{name}{join}{modname}{join}{SITE_NAME}\",\"show_title\":\"[\\u7b2c{page}\\u9875{join}]{title}{join}{catname}{join}{modname}{join}{SITE_NAME}\"}}', 0);

-- --------------------------------------------------------

--
-- 表的结构 `dr_1_share_index`
--

CREATE TABLE `dr_1_share_index` (
  `id` int(10) UNSIGNED NOT NULL,
  `mid` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '模块目录'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='共享模块内容索引表';

--
-- 转存表中的数据 `dr_1_share_index`
--

INSERT INTO `dr_1_share_index` (`id`, `mid`) VALUES
(1, 'news'),
(2, 'news'),
(3, 'news'),
(4, 'news'),
(5, 'news'),
(6, 'news'),
(7, 'news'),
(8, 'news'),
(9, 'news'),
(10, 'news'),
(11, 'news'),
(12, 'news'),
(13, 'news'),
(14, 'news'),
(15, 'news'),
(16, 'news'),
(17, 'news'),
(18, 'news'),
(19, 'news'),
(20, 'news'),
(21, 'news'),
(22, 'news'),
(23, 'news'),
(24, 'news');

-- --------------------------------------------------------

--
-- 表的结构 `dr_admin`
--

CREATE TABLE `dr_admin` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `uid` int(10) UNSIGNED NOT NULL COMMENT '管理员uid',
  `setting` text COLLATE utf8mb4_unicode_ci COMMENT '相关配置',
  `usermenu` text COLLATE utf8mb4_unicode_ci COMMENT '自定义面板菜单，序列化数组格式',
  `history` text COLLATE utf8mb4_unicode_ci COMMENT '历史菜单，序列化数组格式'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='管理员表';

--
-- 转存表中的数据 `dr_admin`
--

INSERT INTO `dr_admin` (`id`, `uid`, `setting`, `usermenu`, `history`) VALUES
(1, 1, '{\"admin_min\":0}', '', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `dr_admin_login`
--

CREATE TABLE `dr_admin_login` (
  `id` int(10) UNSIGNED NOT NULL,
  `uid` mediumint(8) UNSIGNED DEFAULT NULL COMMENT '会员uid',
  `loginip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '登录Ip',
  `logintime` int(10) UNSIGNED NOT NULL COMMENT '登录时间',
  `useragent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '客户端信息'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='登录日志记录';

--
-- 转存表中的数据 `dr_admin_login`
--

INSERT INTO `dr_admin_login` (`id`, `uid`, `loginip`, `logintime`, `useragent`) VALUES
(1, 1, '127.0.0.1', 1646733928, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36 Edg/99.0.1150.30'),
(2, 1, '127.0.0.1', 1646734131, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36 Edg/99.0.1150.30'),
(3, 1, '127.0.0.1', 1646738287, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36 Edg/99.0.1150.30');

-- --------------------------------------------------------

--
-- 表的结构 `dr_admin_menu`
--

CREATE TABLE `dr_admin_menu` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `pid` smallint(5) UNSIGNED NOT NULL COMMENT '上级菜单id',
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '菜单语言名称',
  `site` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '站点归属',
  `uri` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'uri字符串',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '外链地址',
  `mark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '菜单标识',
  `hidden` tinyint(1) UNSIGNED DEFAULT NULL COMMENT '是否隐藏',
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '图标标示',
  `displayorder` int(5) DEFAULT NULL COMMENT '排序值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='后台菜单表';

--
-- 转存表中的数据 `dr_admin_menu`
--

INSERT INTO `dr_admin_menu` (`id`, `pid`, `name`, `site`, `uri`, `url`, `mark`, `hidden`, `icon`, `displayorder`) VALUES
(1, 0, '首页', '', '', '', 'home', 0, 'fa fa-home', 0),
(2, 1, '我的面板', '', '', '', 'home-my', 0, 'fa fa-home', 0),
(3, 2, '后台首页', '', 'home/main', '', '', 0, 'fa fa-home', 0),
(4, 2, '资料修改', '', 'api/my', '', '', 0, 'fa fa-user', 0),
(5, 2, '系统更新', '', 'cache/index', '', '', 0, 'fa fa-refresh', 0),
(6, 2, '应用市场', '', 'api/app', '', '', 0, 'fa fa-puzzle-piece', 0),
(7, 0, '系统', '', '', '', 'system', 0, 'fa fa-globe', 0),
(8, 7, '系统维护', '', '', '', 'system-wh', 0, 'fa fa-cog', 0),
(9, 8, '系统环境', '', 'system/index', '', '', 0, 'fa fa-cog', 0),
(10, 8, '系统缓存', '', 'system_cache/index', '', '', 0, 'fa fa-clock-o', 0),
(11, 8, '附件设置', '', 'attachment/index', '', '', 0, 'fa fa-folder', 0),
(12, 8, '存储策略', '', 'attachment/remote_index', '', '', 0, 'fa fa-cloud', 0),
(13, 8, '短信设置', '', 'sms/index', '', '', 0, 'fa fa-envelope', 0),
(14, 8, '邮件设置', '', 'email/index', '', '', 0, 'fa fa-envelope-open', 0),
(15, 8, '系统提醒', '', 'notice/index', '', '', 0, 'fa fa-bell', 0),
(16, 8, '系统体检', '', 'check/index', '', '', 0, 'fa fa-wrench', 0),
(17, 7, '日志管理', '', '', '', 'system-log', 0, 'fa fa-calendar', 0),
(18, 17, '系统日志', '', 'error/index', '', '', 0, 'fa fa-shield', 0),
(19, 17, '操作日志', '', 'system_log/index', '', '', 0, 'fa fa-calendar', 0),
(20, 17, '短信日志', '', 'sms_log/index', '', '', 0, 'fa fa-envelope', 0),
(21, 17, '邮件日志', '', 'email_log/index', '', '', 0, 'fa fa-envelope-open', 0),
(22, 0, '设置', '', '', '', 'config', 0, 'fa fa-cogs', 0),
(23, 22, '网站设置', '', '', '', 'config-web', 0, 'fa fa-cog', 0),
(24, 23, '网站设置', '', 'site_config/index', '', '', 0, 'fa fa-cog', 0),
(25, 23, '网站信息', '', 'site_param/index', '', '', 0, 'fa fa-edit', 0),
(26, 23, '手机设置', '', 'site_mobile/index', '', '', 0, 'fa fa-mobile', 0),
(27, 23, '域名绑定', '', 'site_domain/index', '', '', 0, 'fa fa-globe', 0),
(28, 23, '图片设置', '', 'site_image/index', '', '', 0, 'fa fa-photo', 0),
(29, 22, '内容设置', '', '', '', 'config-content', 0, 'fa fa-navicon', 0),
(30, 29, '创建模块', '', 'module_create/index', '', '', 0, 'fa fa-plus', 0),
(31, 29, '模块管理', '', 'module/index', '', '', 0, 'fa fa-gears', 0),
(32, 29, '模块搜索', '', 'module_search/index', '', '', 0, 'fa fa-search', 0),
(33, 22, 'SEO设置', '', '', '', 'config-seo', 0, 'fa fa-internet-explorer', 0),
(34, 33, '站点SEO', '', 'seo_site/index', '', '', 0, 'fa fa-cog', 0),
(35, 33, '模块SEO', '', 'seo_module/index', '', '', 0, 'fa fa-th-large', 0),
(36, 33, '栏目SEO', '', 'seo_category/index', '', '', 0, 'fa fa-reorder', 0),
(37, 33, 'URL规则', '', 'urlrule/index', '', '', 0, 'fa fa-link', 0),
(38, 33, '伪静态解析', '', 'urlrule/rewrite_index', '', '', 0, 'bi bi-code-square', 0),
(39, 0, '权限', '', '', '', 'auth', 0, 'fa fa-user-circle', 0),
(40, 39, '后台权限', '', '', '', 'auth-admin', 0, 'fa fa-cog', 0),
(41, 40, '后台菜单', '', 'menu/index', '', '', 0, 'fa fa-list-alt', 0),
(42, 40, '简化菜单', '', 'min_menu/index', '', '', 0, 'fa fa-list', 0),
(43, 40, '角色权限', '', 'role/index', '', '', 0, 'fa fa-users', 0),
(44, 40, '角色账号', '', 'root/index', '', '', 0, 'fa fa-user', 0),
(45, 0, '内容', '', '', '', 'content', 0, 'fa fa-th-large', 0),
(46, 45, '内容管理', '', '', '', 'content-module', 0, 'fa fa-th-large', 0),
(47, 46, '共享栏目', '', 'category/index', '', '', 0, 'fa fa-reorder', 0),
(48, 45, '内容审核', '', '', '', 'content-verify', 0, 'fa fa-edit', 0),
(49, 0, '界面', '', '', '', 'code', 0, 'fa fa-html5', 0),
(50, 49, '模板管理', '', '', '', 'code-html', 0, 'fa fa-home', 0),
(51, 50, '电脑模板', '', 'tpl_pc/index', '', '', 0, 'fa fa-desktop', 0),
(52, 50, '手机模板', '', 'tpl_mobile/index', '', '', 0, 'fa fa-mobile', 0),
(53, 49, '风格管理', '', '', '', 'code-css', 0, 'fa fa-css3', 99),
(54, 53, '系统文件', '', 'system_theme/index', '', '', 0, 'fa fa-chrome', 0),
(55, 53, '网站风格', '', 'theme/index', '', '', 0, 'fa fa-photo', 0),
(56, 0, '应用', '', '', '', 'app', 0, 'fa fa-puzzle-piece', 0),
(57, 56, '应用插件', '', '', '', 'app-plugin', 0, 'fa fa-puzzle-piece', 0),
(58, 57, '应用管理', '', 'cloud/local', '', '', 0, 'fa fa-folder', 0),
(59, 57, '联动菜单', '', 'linkage/index', '', '', 0, 'fa fa-columns', 0),
(60, 57, '任务队列', '', 'cron/index', '', '', 0, 'fa fa-indent', 0),
(61, 57, '附件管理', '', 'attachments/index', '', '', 0, 'fa fa-folder', 0),
(62, 56, '模板调用工具', '', '', '', 'app-mbdy', 0, 'fa fa-tag', 0),
(63, 62, '字段调用标签', '', 'mbdy/home/index', '', '', 0, 'fa fa-list', 0),
(64, 62, '页面标签调用', '', 'mbdy/page/index', '', '', 0, 'fa fa-list', 0),
(65, 62, '循环标签调用', '', 'mbdy/loop/index', '', '', 0, 'fa fa-list', 0),
(66, 62, '内容搜索条件', '', 'mbdy/search/index', '', '', 0, 'fa fa-search', 0),
(67, 0, '服务', '', '', '', 'cloud', 0, 'fa fa-cloud', 99),
(68, 67, '网站管理', '', '', '', 'cloud-dayrui', 0, 'fa fa-cloud', 0),
(69, 68, '我的网站', '', 'cloud/index', '', '', 0, 'fa fa-cog', 0),
(70, 68, '服务工单', '', 'cloud/service', '', '', 0, 'fa fa-user-md', 0),
(71, 68, '应用商城', '', 'cloud/app', '', '', 0, 'fa fa-puzzle-piece', 0),
(72, 68, '模板商城', '', 'cloud/template', '', '', 0, 'fa fa-html5', 0),
(73, 68, '版本升级', '', 'cloud/update', '', '', 0, 'fa fa-refresh', 0),
(74, 68, '文件对比', '', 'cloud/bf', '', '', 0, 'fa fa-code', 0),
(75, 46, '文章管理', '', 'news/home/index', '', 'module-news', 0, 'fa fa-sticky-note', -1),
(76, 48, '文章审核', '', 'news/verify/index', '', 'verify-module-news', 0, 'fa fa-sticky-note', -1),
(77, 56, '开发者工具', '', '', '', 'app-dever', 0, 'fa fa-code', 0),
(78, 77, '应用控制器', '', 'dever/home/index', '', 'app-dever-78', 0, 'fa fa-code', 0),
(79, 77, '字段控件代码', '', 'dever/field/index', '', 'app-dever-79', 0, 'fa fa-table', 0),
(80, 77, '导出模块配置', '', 'dever/module/index', '', 'app-dever-80', 0, 'fa fa-cogs', 0),
(81, 77, '数据表字典', '', 'dever/db/index', '', 'app-dever-81', 0, 'fa fa-database', 0),
(82, 77, '调试变量记录', '', 'dever/debug/index', '', 'app-dever-82', 0, 'fa fa-bug', 0),
(83, 77, '后台HTML特效', '', 'dever/html/index', '', 'app-dever-83', 0, 'fa fa-html5', 0),
(84, 57, '迅睿工具箱', '', 'tool/home/index', '', 'app-tool-84', 0, 'fa fa-cog', 0);

-- --------------------------------------------------------

--
-- 表的结构 `dr_admin_min_menu`
--

CREATE TABLE `dr_admin_min_menu` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `pid` smallint(5) UNSIGNED NOT NULL COMMENT '上级菜单id',
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '菜单语言名称',
  `site` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '站点归属',
  `uri` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'uri字符串',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '外链地址',
  `mark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '菜单标识',
  `hidden` tinyint(1) UNSIGNED DEFAULT NULL COMMENT '是否隐藏',
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '图标标示',
  `displayorder` int(5) DEFAULT NULL COMMENT '排序值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='后台简化菜单表';

--
-- 转存表中的数据 `dr_admin_min_menu`
--

INSERT INTO `dr_admin_min_menu` (`id`, `pid`, `name`, `site`, `uri`, `url`, `mark`, `hidden`, `icon`, `displayorder`) VALUES
(1, 0, '我的面板', '', '', '', 'home', 0, 'fa fa-home', 0),
(2, 1, '后台首页', '', 'home/main', '', '', 0, 'fa fa-home', 0),
(3, 1, '资料修改', '', 'api/my', '', '', 0, 'fa fa-user', 0),
(4, 1, '网站设置', '', 'site_param/index', '', '', 0, 'fa fa-cog', 0),
(5, 1, '附件设置', '', 'attachment/index', '', '', 0, 'fa fa-folder', 0),
(6, 1, '图片设置', '', 'site_image/index', '', '', 0, 'fa fa-photo', 0),
(7, 0, 'SEO设置', '', '', '', 'config-seo', 0, 'fa fa-internet-explorer', 0),
(8, 7, '站点SEO', '', 'seo_site/index', '', '', 0, 'fa fa-cog', 0),
(9, 7, '模块SEO', '', 'seo_module/index', '', '', 0, 'fa fa-gears', 0),
(10, 7, '栏目SEO', '', 'seo_category/index', '', '', 0, 'fa fa-reorder', 0),
(11, 7, 'URL规则', '', 'urlrule/index', '', '', 0, 'fa fa-link', 0),
(12, 0, '内容管理', '', '', '', 'content-module', 0, 'fa fa-th-large', 0),
(13, 0, '应用插件', '', '', '', 'app-plugin', 0, 'fa fa-puzzle-piece', 0),
(14, 13, '联动菜单', '', 'linkage/index', '', '', 0, 'fa fa-columns', 0),
(15, 13, '附件管理', '', 'attachments/index', '', '', 0, 'fa fa-folder', 0),
(16, 12, '文章管理', '', 'news/home/index', '', 'module-news', 0, 'fa fa-sticky-note', -1);

-- --------------------------------------------------------

--
-- 表的结构 `dr_admin_notice`
--

CREATE TABLE `dr_admin_notice` (
  `id` int(10) NOT NULL COMMENT 'id',
  `site` int(5) NOT NULL COMMENT '站点id',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '提醒类型：系统、内容、会员、应用',
  `msg` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '提醒内容说明',
  `uri` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '对应的URI',
  `to_rid` smallint(5) NOT NULL COMMENT '指定角色组',
  `to_uid` int(10) NOT NULL COMMENT '指定管理员',
  `status` tinyint(1) NOT NULL COMMENT '未处理0，1已查看，2处理中，3处理完成',
  `uid` int(10) NOT NULL COMMENT '申请人',
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '申请人',
  `op_uid` int(10) NOT NULL COMMENT '处理人',
  `op_username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '处理人',
  `updatetime` int(10) NOT NULL COMMENT '处理时间',
  `inputtime` int(10) NOT NULL COMMENT '提醒时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='后台提醒表';

-- --------------------------------------------------------

--
-- 表的结构 `dr_admin_role`
--

CREATE TABLE `dr_admin_role` (
  `id` smallint(5) NOT NULL,
  `site` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '允许管理的站点，序列化数组格式',
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '角色组语言名称',
  `system` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '系统权限',
  `module` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '模块权限',
  `application` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '应用权限'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='后台角色权限表';

--
-- 转存表中的数据 `dr_admin_role`
--

INSERT INTO `dr_admin_role` (`id`, `site`, `name`, `system`, `module`, `application`) VALUES
(1, '', '超级管理员', '', '', ''),
(2, '', '网站编辑员', '', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `dr_admin_role_index`
--

CREATE TABLE `dr_admin_role_index` (
  `id` smallint(5) NOT NULL,
  `uid` mediumint(8) UNSIGNED DEFAULT NULL COMMENT '会员uid',
  `roleid` mediumint(8) UNSIGNED DEFAULT NULL COMMENT '角色组id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='后台角色组分配表';

--
-- 转存表中的数据 `dr_admin_role_index`
--

INSERT INTO `dr_admin_role_index` (`id`, `uid`, `roleid`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- 表的结构 `dr_admin_setting`
--

CREATE TABLE `dr_admin_setting` (
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='系统属性参数表';

-- --------------------------------------------------------

--
-- 表的结构 `dr_app_dever_field`
--

CREATE TABLE `dr_app_dever_field` (
  `id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='开发者工具空表';

-- --------------------------------------------------------

--
-- 表的结构 `dr_attachment`
--

CREATE TABLE `dr_attachment` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `uid` mediumint(8) UNSIGNED NOT NULL COMMENT '会员id',
  `author` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '会员',
  `siteid` mediumint(5) UNSIGNED NOT NULL COMMENT '站点id',
  `related` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '相关表标识',
  `tableid` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '附件副表id',
  `download` mediumint(8) NOT NULL DEFAULT '0' COMMENT '无用保留',
  `filesize` int(10) UNSIGNED NOT NULL COMMENT '文件大小',
  `fileext` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文件扩展名',
  `filemd5` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文件md5值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='附件表';

-- --------------------------------------------------------

--
-- 表的结构 `dr_attachment_data`
--

CREATE TABLE `dr_attachment_data` (
  `id` mediumint(8) UNSIGNED NOT NULL COMMENT '附件id',
  `uid` mediumint(8) UNSIGNED NOT NULL DEFAULT '0' COMMENT '会员id',
  `author` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '会员',
  `related` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '相关表标识',
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '原文件名',
  `fileext` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文件扩展名',
  `filesize` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '文件大小',
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '服务器路径',
  `remote` tinyint(2) UNSIGNED NOT NULL DEFAULT '0' COMMENT '远程附件id',
  `attachinfo` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '附件信息',
  `inputtime` int(10) UNSIGNED NOT NULL COMMENT '入库时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='附件已归档表';

-- --------------------------------------------------------

--
-- 表的结构 `dr_attachment_remote`
--

CREATE TABLE `dr_attachment_remote` (
  `id` tinyint(2) UNSIGNED NOT NULL,
  `type` tinyint(2) NOT NULL COMMENT '类型',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名称',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '访问地址',
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '参数值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='远程附件表';

-- --------------------------------------------------------

--
-- 表的结构 `dr_attachment_unused`
--

CREATE TABLE `dr_attachment_unused` (
  `id` mediumint(8) UNSIGNED NOT NULL COMMENT '附件id',
  `uid` mediumint(8) UNSIGNED NOT NULL DEFAULT '0' COMMENT '会员id',
  `author` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '会员',
  `siteid` mediumint(5) UNSIGNED NOT NULL COMMENT '站点id',
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '原文件名',
  `fileext` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文件扩展名',
  `filesize` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '文件大小',
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '服务器路径',
  `remote` tinyint(2) UNSIGNED NOT NULL DEFAULT '0' COMMENT '远程附件id',
  `attachinfo` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '附件信息',
  `inputtime` int(10) UNSIGNED NOT NULL COMMENT '入库时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='未使用的附件表';

-- --------------------------------------------------------

--
-- 表的结构 `dr_cron`
--

CREATE TABLE `dr_cron` (
  `id` int(10) NOT NULL,
  `site` int(10) NOT NULL COMMENT '站点',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '类型',
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '参数值',
  `status` tinyint(1) UNSIGNED NOT NULL COMMENT '状态',
  `error` text COLLATE utf8mb4_unicode_ci COMMENT '错误信息',
  `updatetime` int(10) UNSIGNED NOT NULL COMMENT '执行时间',
  `inputtime` int(10) UNSIGNED NOT NULL COMMENT '写入时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='任务管理';

-- --------------------------------------------------------

--
-- 表的结构 `dr_field`
--

CREATE TABLE `dr_field` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '字段别名语言',
  `fieldname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '字段名称',
  `fieldtype` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '字段类型',
  `relatedid` smallint(5) UNSIGNED NOT NULL COMMENT '相关id',
  `relatedname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '相关表',
  `isedit` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT '是否可修改',
  `ismain` tinyint(1) UNSIGNED NOT NULL COMMENT '是否主表',
  `issystem` tinyint(1) UNSIGNED NOT NULL COMMENT '是否系统表',
  `ismember` tinyint(1) UNSIGNED NOT NULL COMMENT '是否会员可见',
  `issearch` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT '是否可搜索',
  `disabled` tinyint(1) UNSIGNED NOT NULL COMMENT '禁用？',
  `setting` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '配置信息',
  `displayorder` int(5) NOT NULL COMMENT '排序'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='字段表';

--
-- 转存表中的数据 `dr_field`
--

INSERT INTO `dr_field` (`id`, `name`, `fieldname`, `fieldtype`, `relatedid`, `relatedname`, `isedit`, `ismain`, `issystem`, `ismember`, `issearch`, `disabled`, `setting`, `displayorder`) VALUES
(1, '主题', 'title', 'Text', 1, 'module', 1, 1, 1, 1, 1, 0, '{\"option\":{\"width\":400,\"fieldtype\":\"VARCHAR\",\"fieldlength\":\"255\"},\"validate\":{\"xss\":1,\"required\":1,\"formattr\":\"onblur=\\\"check_title();get_keywords(\'keywords\');\\\"\"}}', 0),
(2, '缩略图', 'thumb', 'File', 1, 'module', 1, 1, 1, 1, 1, 0, '{\"option\":{\"ext\":\"jpg,gif,png\",\"size\":10,\"width\":400,\"fieldtype\":\"VARCHAR\",\"fieldlength\":\"255\"}}', 0),
(3, '关键字', 'keywords', 'Text', 1, 'module', 1, 1, 1, 1, 1, 0, '{\"option\":{\"width\":400,\"fieldtype\":\"VARCHAR\",\"fieldlength\":\"255\"},\"validate\":{\"xss\":1,\"formattr\":\" data-role=\\\"tagsinput\\\"\"}}', 0),
(4, '描述', 'description', 'Textarea', 1, 'module', 1, 1, 1, 1, 1, 0, '{\"option\":{\"width\":500,\"height\":60,\"fieldtype\":\"VARCHAR\",\"fieldlength\":\"255\"},\"validate\":{\"xss\":1,\"filter\":\"dr_filter_description\"}}', 0),
(5, '笔名', 'author', 'Text', 1, 'module', 1, 1, 1, 1, 1, 0, '{\"is_right\":1,\"option\":{\"width\":200,\"fieldtype\":\"VARCHAR\",\"fieldlength\":\"255\",\"value\":\"{name}\"},\"validate\":{\"xss\":1}}', 0),
(6, '内容', 'content', 'Editor', 1, 'module', 1, 0, 1, 1, 1, 0, '{\"option\":{\"mode\":1,\"show_bottom_boot\":1,\"div2p\":1,\"width\":\"100%\",\"height\":400},\"validate\":{\"xss\":1,\"required\":1}}', 0),
(7, '友情链接', 'yqlj', 'Ftable', 1, 'site', 1, 1, 0, 1, 0, 0, '{\"option\":{\"is_add\":\"1\",\"is_first_hang\":\"0\",\"count\":\"\",\"first_cname\":\"\",\"hang\":{\"1\":{\"name\":\"\"},\"2\":{\"name\":\"\"},\"3\":{\"name\":\"\"},\"4\":{\"name\":\"\"},\"5\":{\"name\":\"\"}},\"field\":{\"1\":{\"type\":\"1\",\"name\":\"网站名称\",\"width\":\"200\",\"option\":\"\"},\"2\":{\"type\":\"1\",\"name\":\"网站地址\",\"width\":\"\",\"option\":\"\"},\"3\":{\"type\":\"0\",\"name\":\"\",\"width\":\"\",\"option\":\"\"},\"4\":{\"type\":\"0\",\"name\":\"\",\"width\":\"\",\"option\":\"\"},\"5\":{\"type\":\"0\",\"name\":\"\",\"width\":\"\",\"option\":\"\"},\"6\":{\"type\":\"0\",\"name\":\"\",\"width\":\"\",\"option\":\"\"},\"7\":{\"type\":\"0\",\"name\":\"\",\"width\":\"\",\"option\":\"\"},\"8\":{\"type\":\"0\",\"name\":\"\",\"width\":\"\",\"option\":\"\"},\"9\":{\"type\":\"0\",\"name\":\"\",\"width\":\"\",\"option\":\"\"},\"10\":{\"type\":\"0\",\"name\":\"\",\"width\":\"\",\"option\":\"\"}},\"width\":\"\",\"height\":\"\",\"css\":\"\"},\"validate\":{\"required\":\"0\",\"pattern\":\"\",\"errortips\":\"\",\"xss\":\"1\",\"check\":\"\",\"filter\":\"\",\"tips\":\"\",\"formattr\":\"\"},\"is_right\":\"0\"}', 0),
(8, '幻灯图片', 'hdtp', 'Ftable', 1, 'site', 1, 1, 0, 1, 0, 0, '{\"option\":{\"is_add\":\"1\",\"is_first_hang\":\"0\",\"count\":\"\",\"first_cname\":\"\",\"hang\":{\"1\":{\"name\":\"\"},\"2\":{\"name\":\"\"},\"3\":{\"name\":\"\"},\"4\":{\"name\":\"\"},\"5\":{\"name\":\"\"}},\"field\":{\"1\":{\"type\":\"3\",\"name\":\"图片\",\"width\":\"200\",\"option\":\"\"},\"2\":{\"type\":\"1\",\"name\":\"名称\",\"width\":\"200\",\"option\":\"\"},\"3\":{\"type\":\"1\",\"name\":\"跳转地址\",\"width\":\"\",\"option\":\"\"},\"4\":{\"type\":\"0\",\"name\":\"\",\"width\":\"\",\"option\":\"\"},\"5\":{\"type\":\"0\",\"name\":\"\",\"width\":\"\",\"option\":\"\"},\"6\":{\"type\":\"0\",\"name\":\"\",\"width\":\"\",\"option\":\"\"},\"7\":{\"type\":\"0\",\"name\":\"\",\"width\":\"\",\"option\":\"\"},\"8\":{\"type\":\"0\",\"name\":\"\",\"width\":\"\",\"option\":\"\"},\"9\":{\"type\":\"0\",\"name\":\"\",\"width\":\"\",\"option\":\"\"},\"10\":{\"type\":\"0\",\"name\":\"\",\"width\":\"\",\"option\":\"\"}},\"width\":\"\",\"height\":\"\",\"css\":\"\"},\"validate\":{\"required\":\"0\",\"pattern\":\"\",\"errortips\":\"\",\"xss\":\"1\",\"check\":\"\",\"filter\":\"\",\"tips\":\"\",\"formattr\":\"\"},\"is_right\":\"0\"}', 0),
(9, '缩略图', 'thumb', 'File', 0, 'category-share', 1, 1, 1, 1, 1, 0, '{\"option\":{\"ext\":\"jpg,gif,png,jpeg\",\"size\":10,\"input\":1,\"attachment\":0}}', 0),
(10, '栏目内容', 'content', 'Ueditor', 0, 'category-share', 1, 1, 1, 1, 1, 0, '{\"option\":{\"mode\":1,\"show_bottom_boot\":1,\"div2p\":1,\"width\":\"100%\",\"height\":400},\"validate\":{\"xss\":1,\"required\":1}}', 0);

-- --------------------------------------------------------

--
-- 表的结构 `dr_linkage`
--

CREATE TABLE `dr_linkage` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '菜单名称',
  `type` tinyint(1) UNSIGNED NOT NULL,
  `code` char(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='联动菜单表';

--
-- 转存表中的数据 `dr_linkage`
--

INSERT INTO `dr_linkage` (`id`, `name`, `type`, `code`) VALUES
(1, '中国地区', 0, 'address');

-- --------------------------------------------------------

--
-- 表的结构 `dr_linkage_data_1`
--

CREATE TABLE `dr_linkage_data_1` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `site` mediumint(5) UNSIGNED NOT NULL COMMENT '站点id',
  `pid` mediumint(8) UNSIGNED NOT NULL DEFAULT '0' COMMENT '上级id',
  `pids` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '所有上级id',
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '栏目名称',
  `cname` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '别名',
  `child` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '是否有下级',
  `hidden` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '前端隐藏',
  `childids` text COLLATE utf8mb4_unicode_ci COMMENT '下级所有id',
  `displayorder` mediumint(8) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='联动菜单数据表';

--
-- 转存表中的数据 `dr_linkage_data_1`
--

INSERT INTO `dr_linkage_data_1` (`id`, `site`, `pid`, `pids`, `name`, `cname`, `child`, `hidden`, `childids`, `displayorder`) VALUES
(1, 1, 0, '0', '北京', 'bj', 0, 0, '1', 0),
(2, 1, 0, '0', '成都', 'cd', 0, 0, '2', 0);

-- --------------------------------------------------------

--
-- 表的结构 `dr_mail_smtp`
--

CREATE TABLE `dr_mail_smtp` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `host` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `port` mediumint(8) UNSIGNED NOT NULL,
  `displayorder` smallint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='邮件账户表';

-- --------------------------------------------------------

--
-- 表的结构 `dr_member`
--

CREATE TABLE `dr_member` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '邮箱地址',
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '手机号码',
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '加密密码',
  `login_attr` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '登录附加验证字符',
  `salt` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '随机加密码',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '姓名',
  `money` decimal(10,2) UNSIGNED NOT NULL COMMENT 'RMB',
  `freeze` decimal(10,2) UNSIGNED NOT NULL COMMENT '冻结RMB',
  `spend` decimal(10,2) UNSIGNED NOT NULL COMMENT '消费RMB总额',
  `score` int(10) UNSIGNED NOT NULL COMMENT '积分',
  `experience` int(10) UNSIGNED NOT NULL COMMENT '经验值',
  `regip` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '注册ip',
  `regtime` int(10) UNSIGNED NOT NULL COMMENT '注册时间',
  `randcode` mediumint(6) UNSIGNED NOT NULL COMMENT '随机验证码'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员表';

--
-- 转存表中的数据 `dr_member`
--

INSERT INTO `dr_member` (`id`, `email`, `phone`, `username`, `password`, `login_attr`, `salt`, `name`, `money`, `freeze`, `spend`, `score`, `experience`, `regip`, `regtime`, `randcode`) VALUES
(1, 'admin@admin.com', '', 'admin', '6ef8fe6670b8e206823ac8eb3cbaea14', '', 'c8fbbc86ab', '创始人', '1000000.00', '0.00', '0.00', 1000000, 1000000, '', 1646733922, 0);

-- --------------------------------------------------------

--
-- 表的结构 `dr_member_data`
--

CREATE TABLE `dr_member_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_admin` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '是否是管理员',
  `is_lock` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '账号锁定标识',
  `is_verify` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '审核标识',
  `is_mobile` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '手机认证标识',
  `is_email` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '邮箱认证标识',
  `is_avatar` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '头像上传标识',
  `is_complete` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '完善资料标识'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员表';

--
-- 转存表中的数据 `dr_member_data`
--

INSERT INTO `dr_member_data` (`id`, `is_admin`, `is_lock`, `is_verify`, `is_mobile`, `is_email`, `is_avatar`, `is_complete`) VALUES
(1, 1, 0, 1, 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `dr_module`
--

CREATE TABLE `dr_module` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `site` text COLLATE utf8mb4_unicode_ci COMMENT '站点划分',
  `dirname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '目录名称',
  `share` tinyint(1) UNSIGNED DEFAULT NULL COMMENT '是否共享模块',
  `setting` text COLLATE utf8mb4_unicode_ci COMMENT '配置信息',
  `comment` text COLLATE utf8mb4_unicode_ci COMMENT '评论信息',
  `disabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT '禁用？',
  `displayorder` smallint(5) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='模块表';

--
-- 转存表中的数据 `dr_module`
--

INSERT INTO `dr_module` (`id`, `site`, `dirname`, `share`, `setting`, `comment`, `disabled`, `displayorder`) VALUES
(1, '{\"1\":{\"html\":0,\"theme\":\"default\",\"domain\":\"\",\"template\":\"default\"}}', 'news', 1, '{\"order\":\"displayorder DESC,updatetime DESC\",\"verify_msg\":\"\",\"delete_msg\":\"\",\"list_field\":{\"title\":{\"use\":\"1\",\"order\":\"1\",\"name\":\"主题\",\"width\":\"\",\"func\":\"title\"},\"catid\":{\"use\":\"1\",\"order\":\"2\",\"name\":\"栏目\",\"width\":\"120\",\"func\":\"catid\"},\"author\":{\"use\":\"1\",\"order\":\"3\",\"name\":\"作者\",\"width\":\"100\",\"func\":\"author\"},\"updatetime\":{\"use\":\"1\",\"order\":\"4\",\"name\":\"更新时间\",\"width\":\"160\",\"func\":\"datetime\"}},\"comment_list_field\":{\"content\":{\"use\":\"1\",\"order\":\"1\",\"name\":\"评论\",\"width\":\"\",\"func\":\"comment\"},\"author\":{\"use\":\"1\",\"order\":\"3\",\"name\":\"作者\",\"width\":\"100\",\"func\":\"author\"},\"inputtime\":{\"use\":\"1\",\"order\":\"4\",\"name\":\"评论时间\",\"width\":\"160\",\"func\":\"datetime\"}},\"flag\":null,\"param\":null,\"search\":{\"use\":\"1\",\"field\":\"title,keywords\",\"total\":\"500\",\"length\":\"4\",\"param_join\":\"-\",\"param_rule\":\"0\",\"param_field\":\"\",\"param_join_field\":[\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"param_join_default_value\":\"0\"}}', '', 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `dr_site`
--

CREATE TABLE `dr_site` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '站点名称',
  `domain` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '站点域名',
  `setting` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '站点配置',
  `disabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT '禁用？',
  `displayorder` smallint(5) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='站点表';

--
-- 转存表中的数据 `dr_site`
--

INSERT INTO `dr_site` (`id`, `name`, `domain`, `setting`, `disabled`, `displayorder`) VALUES
(1, '我的网站', 'xr.cc', '{\"webpath\":null}', 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `dr_urlrule`
--

CREATE TABLE `dr_urlrule` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `type` tinyint(1) UNSIGNED NOT NULL COMMENT '规则类型',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '规则名称',
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '详细规则'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='URL规则表';

--
-- 转存表中的数据 `dr_urlrule`
--

INSERT INTO `dr_urlrule` (`id`, `type`, `name`, `value`) VALUES
(1, 3, '共享栏目测试规则', '{\"list\":\"list-{dirname}.html\",\"list_page\":\"list-{dirname}-{page}.html\",\"show\":\"show-{id}.html\",\"show_page\":\"show-{id}-{page}.html\",\"catjoin\":\"\\/\"}'),
(2, 2, '共享模块测试规则', '{\"search\":\"{modname}\\/search.html\",\"search_page\":\"{modname}\\/search\\/{param}.html\",\"catjoin\":\"\\/\"}'),
(3, 1, '独立模块测试规则', '{\"module\":\"{modname}.html\",\"list\":\"{modname}\\/list\\/{id}.html\",\"list_page\":\"{modname}\\/list\\/{id}\\/{page}.html\",\"show\":\"{modname}\\/show\\/{id}.html\",\"show_page\":\"{modname}\\/show\\/{id}\\/{page}.html\",\"search\":\"{modname}\\/search.html\",\"search_page\":\"{modname}\\/search\\/{param}.html\",\"catjoin\":\"\\/\"}');

--
-- 转储表的索引
--

--
-- 表的索引 `dr_1_news`
--
ALTER TABLE `dr_1_news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `catid` (`catid`),
  ADD KEY `link_id` (`link_id`),
  ADD KEY `status` (`status`),
  ADD KEY `updatetime` (`updatetime`),
  ADD KEY `hits` (`hits`),
  ADD KEY `category` (`catid`,`status`),
  ADD KEY `displayorder` (`displayorder`);

--
-- 表的索引 `dr_1_news_category`
--
ALTER TABLE `dr_1_news_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `show` (`show`),
  ADD KEY `disabled` (`disabled`),
  ADD KEY `ismain` (`ismain`),
  ADD KEY `module` (`pid`,`displayorder`,`id`);

--
-- 表的索引 `dr_1_news_category_data`
--
ALTER TABLE `dr_1_news_category_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `catid` (`catid`);

--
-- 表的索引 `dr_1_news_data_0`
--
ALTER TABLE `dr_1_news_data_0`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `catid` (`catid`);

--
-- 表的索引 `dr_1_news_draft`
--
ALTER TABLE `dr_1_news_draft`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `cid` (`cid`),
  ADD KEY `catid` (`catid`),
  ADD KEY `inputtime` (`inputtime`);

--
-- 表的索引 `dr_1_news_flag`
--
ALTER TABLE `dr_1_news_flag`
  ADD KEY `flag` (`flag`,`id`,`uid`),
  ADD KEY `catid` (`catid`);

--
-- 表的索引 `dr_1_news_hits`
--
ALTER TABLE `dr_1_news_hits`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `day_hits` (`day_hits`),
  ADD KEY `week_hits` (`week_hits`),
  ADD KEY `month_hits` (`month_hits`),
  ADD KEY `year_hits` (`year_hits`);

--
-- 表的索引 `dr_1_news_index`
--
ALTER TABLE `dr_1_news_index`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `catid` (`catid`),
  ADD KEY `status` (`status`),
  ADD KEY `inputtime` (`inputtime`);

--
-- 表的索引 `dr_1_news_recycle`
--
ALTER TABLE `dr_1_news_recycle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `cid` (`cid`),
  ADD KEY `catid` (`catid`),
  ADD KEY `inputtime` (`inputtime`);

--
-- 表的索引 `dr_1_news_search`
--
ALTER TABLE `dr_1_news_search`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `catid` (`catid`),
  ADD KEY `keyword` (`keyword`),
  ADD KEY `inputtime` (`inputtime`);

--
-- 表的索引 `dr_1_news_time`
--
ALTER TABLE `dr_1_news_time`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `catid` (`catid`),
  ADD KEY `posttime` (`posttime`),
  ADD KEY `inputtime` (`inputtime`);

--
-- 表的索引 `dr_1_news_verify`
--
ALTER TABLE `dr_1_news_verify`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `vid` (`vid`),
  ADD KEY `catid` (`catid`),
  ADD KEY `status` (`status`),
  ADD KEY `inputtime` (`inputtime`),
  ADD KEY `backuid` (`backuid`);

--
-- 表的索引 `dr_1_share_category`
--
ALTER TABLE `dr_1_share_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mid` (`mid`),
  ADD KEY `tid` (`tid`),
  ADD KEY `show` (`show`),
  ADD KEY `disabled` (`disabled`),
  ADD KEY `ismain` (`ismain`),
  ADD KEY `dirname` (`dirname`),
  ADD KEY `module` (`pid`,`displayorder`,`id`);

--
-- 表的索引 `dr_1_share_index`
--
ALTER TABLE `dr_1_share_index`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mid` (`mid`);

--
-- 表的索引 `dr_admin`
--
ALTER TABLE `dr_admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uid` (`uid`);

--
-- 表的索引 `dr_admin_login`
--
ALTER TABLE `dr_admin_login`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `loginip` (`loginip`),
  ADD KEY `logintime` (`logintime`);

--
-- 表的索引 `dr_admin_menu`
--
ALTER TABLE `dr_admin_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `list` (`pid`),
  ADD KEY `displayorder` (`displayorder`),
  ADD KEY `mark` (`mark`),
  ADD KEY `hidden` (`hidden`),
  ADD KEY `uri` (`uri`);

--
-- 表的索引 `dr_admin_min_menu`
--
ALTER TABLE `dr_admin_min_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `list` (`pid`),
  ADD KEY `displayorder` (`displayorder`),
  ADD KEY `mark` (`mark`),
  ADD KEY `hidden` (`hidden`),
  ADD KEY `uri` (`uri`);

--
-- 表的索引 `dr_admin_notice`
--
ALTER TABLE `dr_admin_notice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uri` (`uri`),
  ADD KEY `site` (`site`),
  ADD KEY `status` (`status`),
  ADD KEY `uid` (`uid`),
  ADD KEY `op_uid` (`op_uid`),
  ADD KEY `to_uid` (`to_uid`),
  ADD KEY `to_rid` (`to_rid`),
  ADD KEY `updatetime` (`updatetime`),
  ADD KEY `inputtime` (`inputtime`);

--
-- 表的索引 `dr_admin_role`
--
ALTER TABLE `dr_admin_role`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `dr_admin_role_index`
--
ALTER TABLE `dr_admin_role_index`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `roleid` (`roleid`);

--
-- 表的索引 `dr_admin_setting`
--
ALTER TABLE `dr_admin_setting`
  ADD PRIMARY KEY (`name`);

--
-- 表的索引 `dr_app_dever_field`
--
ALTER TABLE `dr_app_dever_field`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `dr_attachment`
--
ALTER TABLE `dr_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `author` (`author`),
  ADD KEY `relatedtid` (`related`),
  ADD KEY `fileext` (`fileext`),
  ADD KEY `filemd5` (`filemd5`),
  ADD KEY `siteid` (`siteid`);

--
-- 表的索引 `dr_attachment_data`
--
ALTER TABLE `dr_attachment_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inputtime` (`inputtime`),
  ADD KEY `fileext` (`fileext`),
  ADD KEY `remote` (`remote`),
  ADD KEY `author` (`author`),
  ADD KEY `uid` (`uid`);

--
-- 表的索引 `dr_attachment_remote`
--
ALTER TABLE `dr_attachment_remote`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `dr_attachment_unused`
--
ALTER TABLE `dr_attachment_unused`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `inputtime` (`inputtime`),
  ADD KEY `fileext` (`fileext`),
  ADD KEY `remote` (`remote`),
  ADD KEY `author` (`author`);

--
-- 表的索引 `dr_cron`
--
ALTER TABLE `dr_cron`
  ADD PRIMARY KEY (`id`),
  ADD KEY `site` (`site`),
  ADD KEY `type` (`type`),
  ADD KEY `status` (`status`),
  ADD KEY `updatetime` (`updatetime`),
  ADD KEY `inputtime` (`inputtime`);

--
-- 表的索引 `dr_field`
--
ALTER TABLE `dr_field`
  ADD PRIMARY KEY (`id`),
  ADD KEY `list` (`relatedid`,`disabled`,`issystem`);

--
-- 表的索引 `dr_linkage`
--
ALTER TABLE `dr_linkage`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `module` (`id`);

--
-- 表的索引 `dr_linkage_data_1`
--
ALTER TABLE `dr_linkage_data_1`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cname` (`cname`),
  ADD KEY `hidden` (`hidden`),
  ADD KEY `list` (`site`,`displayorder`);

--
-- 表的索引 `dr_mail_smtp`
--
ALTER TABLE `dr_mail_smtp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `displayorder` (`displayorder`);

--
-- 表的索引 `dr_member`
--
ALTER TABLE `dr_member`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`),
  ADD KEY `email` (`email`),
  ADD KEY `phone` (`phone`);

--
-- 表的索引 `dr_member_data`
--
ALTER TABLE `dr_member_data`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `dr_module`
--
ALTER TABLE `dr_module`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dirname` (`dirname`),
  ADD KEY `disabled` (`disabled`),
  ADD KEY `displayorder` (`displayorder`);

--
-- 表的索引 `dr_site`
--
ALTER TABLE `dr_site`
  ADD PRIMARY KEY (`id`),
  ADD KEY `disabled` (`disabled`),
  ADD KEY `displayorder` (`displayorder`);

--
-- 表的索引 `dr_urlrule`
--
ALTER TABLE `dr_urlrule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `dr_1_news`
--
ALTER TABLE `dr_1_news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- 使用表AUTO_INCREMENT `dr_1_news_category`
--
ALTER TABLE `dr_1_news_category`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `dr_1_news_category_data`
--
ALTER TABLE `dr_1_news_category_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `dr_1_news_draft`
--
ALTER TABLE `dr_1_news_draft`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `dr_1_news_index`
--
ALTER TABLE `dr_1_news_index`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- 使用表AUTO_INCREMENT `dr_1_news_recycle`
--
ALTER TABLE `dr_1_news_recycle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `dr_1_news_time`
--
ALTER TABLE `dr_1_news_time`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `dr_1_share_category`
--
ALTER TABLE `dr_1_share_category`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- 使用表AUTO_INCREMENT `dr_1_share_index`
--
ALTER TABLE `dr_1_share_index`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- 使用表AUTO_INCREMENT `dr_admin`
--
ALTER TABLE `dr_admin`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `dr_admin_login`
--
ALTER TABLE `dr_admin_login`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `dr_admin_menu`
--
ALTER TABLE `dr_admin_menu`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- 使用表AUTO_INCREMENT `dr_admin_min_menu`
--
ALTER TABLE `dr_admin_min_menu`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- 使用表AUTO_INCREMENT `dr_admin_notice`
--
ALTER TABLE `dr_admin_notice`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id';

--
-- 使用表AUTO_INCREMENT `dr_admin_role`
--
ALTER TABLE `dr_admin_role`
  MODIFY `id` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `dr_admin_role_index`
--
ALTER TABLE `dr_admin_role_index`
  MODIFY `id` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `dr_app_dever_field`
--
ALTER TABLE `dr_app_dever_field`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `dr_attachment`
--
ALTER TABLE `dr_attachment`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `dr_attachment_remote`
--
ALTER TABLE `dr_attachment_remote`
  MODIFY `id` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `dr_cron`
--
ALTER TABLE `dr_cron`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `dr_field`
--
ALTER TABLE `dr_field`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- 使用表AUTO_INCREMENT `dr_linkage`
--
ALTER TABLE `dr_linkage`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `dr_linkage_data_1`
--
ALTER TABLE `dr_linkage_data_1`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `dr_mail_smtp`
--
ALTER TABLE `dr_mail_smtp`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `dr_member`
--
ALTER TABLE `dr_member`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `dr_member_data`
--
ALTER TABLE `dr_member_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `dr_module`
--
ALTER TABLE `dr_module`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `dr_site`
--
ALTER TABLE `dr_site`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `dr_urlrule`
--
ALTER TABLE `dr_urlrule`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
